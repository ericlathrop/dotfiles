#!/bin/bash

set -euo pipefail

function link_files() {
  destination_folder="$1"

  for file in "${@:2}"; do
    [[ $file == "." ]] && continue
    [[ $file == ".." ]] && continue

    base_name=$(basename "$file")
    dest=$destination_folder/$base_name

    if [ ! -e "$dest" ]; then
      echo "Linking $dest"
      ln -rs "$file" "$dest"
    fi
  done
}

link_files "$HOME" hidden_files/.*
link_files "$HOME/.config" config/*
