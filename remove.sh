#!/bin/bash

set -euo pipefail

function unlink_files() {
  destination_folder="$1"

  for file in "${@:2}"; do
    [[ $file == "." ]] && continue
    [[ $file == ".." ]] && continue

    base_name=$(basename "$file")
    dest=$destination_folder/$base_name

    if [ -L "$dest" ]; then
      echo "Removing $dest"
      rm "$dest"
    fi
  done
}

unlink_files "$HOME" hidden_files/.*
unlink_files "$HOME/.config" config/*
