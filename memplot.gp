set term png small size 800,600

set ylabel "VSZ"
set y2label "%MEM"

set ytics nomirror
set y2tics nomirror in

set yrange [0:*]
set y2range [0:*]

plot input using 3 with lines axes x1y1 title "VSZ", \
     input using 2 with lines axes x1y2 title "%MEM"
