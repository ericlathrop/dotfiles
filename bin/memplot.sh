#!/bin/bash

if [[ $# -ne 2 ]]; then
  echo "Usage: memplot.sh process-name output"
  exit 1
fi

programdir=$(dirname "$0")
process=$1
output=$2

memlog=$(mktemp)
trap "rm -f $memlog" EXIT

first=true

while true; do
  ps -C "$process" -o pid=,%mem=,vsz= >> "$memlog"
  gnuplot -e "input='$memlog'" "$programdir/../memplot.gp" > "$output"
  if $first; then
    xdg-open "$output"
    first=false
  fi
  sleep 1
done
