#!/bin/bash

# In Unity go to Edit->Preferences->External Tools
# Set External Script Editor Args to
#
# $(File):$(Line):$(ProjectPath)
#

IFS=':' read -ra ARGS <<< "$1"
FILE=${ARGS[0]}
LINE=${ARGS[1]}
PROJECTDIR=${ARGS[2]}

if [ -z "$LINE" ]; then
  LINE=1
fi
if [ "$LINE" -lt 1 ]; then
  LINE=1
fi

# echo "$FILE"
# echo "$LINE"
# echo "$PROJECTDIR"
# cd "$PROJECTDIR"
MYDIR="$(dirname "$(realpath "$0")")"
"$MYDIR/vimuxterm" unity +$LINE "$FILE"
