vim.g.mapleader = " "

vim.opt.number = true

vim.opt.ignorecase = true
vim.opt.smartcase = true
vim.opt.inccommand = "split"

vim.opt.clipboard:append('unnamedplus')

vim.opt.listchars = { tab = "»·", trail = "·" }
vim.opt.list = true

vim.opt.expandtab = true
vim.opt.tabstop = 2
vim.opt.shiftwidth = 2

vim.opt.startofline = true

vim.opt.scrolloff = 999
vim.opt.sidescrolloff = 15

vim.opt.undofile = true

vim.keymap.set("n", "<leader>w", ":w<CR>")
vim.keymap.set("n", "<leader>q", ":bd<CR>")

vim.keymap.set("n", "<Left>", ":bprev<CR>")
vim.keymap.set("n", "<Right>", ":bnext<CR>")

vim.keymap.set("n", "<leader>c", ":e ~/.config/nvim/init.lua<CR>")
vim.keymap.set("n", "<leader>l", ":e ~/.config/nvim/lua/")

vim.keymap.set("n", "<C-j>", "<C-w>j")
vim.keymap.set("n", "<C-k>", "<C-w>k")
vim.keymap.set("n", "<C-h>", "<C-w>h")
vim.keymap.set("n", "<C-l>", "<C-w>l")
vim.keymap.set("n", "<Esc><C-j>", "<C-w>J")
vim.keymap.set("n", "<Esc><C-k>", "<C-w>K")
vim.keymap.set("n", "<Esc><C-h>", "<C-w>H")
vim.keymap.set("n", "<Esc><C-l>", "<C-w>L")
vim.keymap.set("n", "<C-p>", "<C-w>p")

vim.keymap.set("n", "<leader>t", ":%s/\\s\\+$//e<CR>")

vim.keymap.set("n", "[l", ":lprevious<CR>")
vim.keymap.set("n", "]l", ":lnext<CR>")
vim.keymap.set("n", "[q", ":cprevious<CR>")
vim.keymap.set("n", "]q", ":cnext<CR>")

vim.keymap.set("n", "<LeftMouse>", "m'<LeftMouse>")

vim.api.nvim_create_autocmd("VimResized", {
  desc = "Keep windows equally resized",
  command = "tabdo wincmd ="
})

require("lazy-bootstrap")
require("lazy").setup(require("lazy-config"))

local pipepath = vim.fn.stdpath("cache") .. "/server.pipe"
if not vim.loop.fs_stat(pipepath) then
  vim.fn.serverstart(pipepath)
end
