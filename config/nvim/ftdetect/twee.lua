vim.api.nvim_create_autocmd({ 'BufRead', 'BufNewFile' }, {
  pattern = "*.twee",
  callback = function()
    vim.bo.filetype = 'twee'
  end
})
