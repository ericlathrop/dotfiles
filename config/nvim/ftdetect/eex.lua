vim.api.nvim_create_autocmd({ 'BufRead', 'BufNewFile' }, {
  pattern = "*.eex",
  callback = function()
    vim.bo.filetype = 'heex'
  end
})
