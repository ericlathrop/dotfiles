vim.keymap.set('n', "<Leader>fb", ":Buffers<CR>")
vim.keymap.set('n', "<Leader>ff", ":Files<CR>")
vim.keymap.set('n', "<Leader>fg", ":GFiles<CR>")
vim.keymap.set('n', "<Leader>fl", ":BLines<CR>")
vim.keymap.set('n', "<Leader>fr", ":RG<CR>")
vim.g.fzf_history_dir = "~/.local/share/fzf-history"
