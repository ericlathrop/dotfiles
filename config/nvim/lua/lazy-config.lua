return {
  {
    "tanvirtin/monokai.nvim",
    config = function()
      require('monokai').setup {}
    end
  },
  "tpope/vim-repeat",
  "tpope/vim-surround",
  {
    "tpope/vim-fugitive",
    config = function()
      vim.keymap.set('n', "<Leader>g", ":Git<CR>")
    end
  },
  "RRethy/nvim-treesitter-endwise",
  {
    'windwp/nvim-autopairs',
    event = "InsertEnter",
    config = true,
  },
  {
    "nvim-treesitter/nvim-treesitter",
    build = ":TSUpdate",
    config = function()
      require("treesitter-config")
    end
  },
  {
    "nvim-treesitter/nvim-treesitter-textobjects",
    dependencies = { "nvim-treesitter/nvim-treesitter" },
    config = function()
      require("treesitter-textobjects-config")
    end
  },
  {
    "ojroques/nvim-bufbar",
    config = function()
      require("bufbar").setup({
        show_bufname = "all"
      })
    end
  },
  {
    "christoomey/vim-tmux-navigator",
    cmd = {
      "TmuxNavigateLeft",
      "TmuxNavigateDown",
      "TmuxNavigateUp",
      "TmuxNavigateRight",
      "TmuxNavigatePrevious",
    },
    keys = {
      { "<c-h>",  "<cmd><C-U>TmuxNavigateLeft<cr>" },
      { "<c-j>",  "<cmd><C-U>TmuxNavigateDown<cr>" },
      { "<c-k>",  "<cmd><C-U>TmuxNavigateUp<cr>" },
      { "<c-l>",  "<cmd><C-U>TmuxNavigateRight<cr>" },
      { "<c-\\>", "<cmd><C-U>TmuxNavigatePrevious<cr>" },
    },
  },
  "vim-scripts/restore_view.vim",
  "junegunn/fzf",
  {
    "junegunn/fzf.vim",
    dependencies = { "junegunn/fzf" },
    config = function()
      require("fzf-config")
    end,
  },
  "mfussenegger/nvim-dap",
  "folke/neodev.nvim",
  {
    "neovim/nvim-lspconfig",
    config = function()
      require("lspconfig-config")
    end,
    dependencies = { "folke/neodev.nvim" },
  },
  {
    "lewis6991/gitsigns.nvim",
    config = function()
      require("gitsigns").setup()
    end
  },
  {
    "L3MON4D3/LuaSnip",
    version = "v2.*",
    build = "make install_jsregexp"
  },
  "saadparwaiz1/cmp_luasnip",
  "hrsh7th/cmp-nvim-lsp",
  "hrsh7th/cmp-buffer",
  "hrsh7th/cmp-path",
  "hrsh7th/cmp-cmdline",
  "f3fora/cmp-spell",
  {
    "hrsh7th/nvim-cmp",
    config = function()
      require("cmp-config")
    end
  },
  {
    'numToStr/Comment.nvim',
    config = function()
      require("Comment").setup()
    end,
    lazy = false,
  }
}
