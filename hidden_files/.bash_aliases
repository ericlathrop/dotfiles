# load all scripts in the bashrc folder
# this is named bash_aliases because Ubuntu's .bashrc always includes that.

command_exists() {
	command -v "$1" >/dev/null 2>&1
}

for file in ~/.dotfiles/bashrc/*
do
	. $file
done
