source ~/.vim/plug.vim
scriptencoding utf-8

" ================ General Config ====================

set number " Line numbers are good
set backspace=indent,eol,start " Allow backspace in insert mode
set history=1000 " Store lots of :cmdline history
set showcmd " Show incomplete cmds down the bottom
set autoread " Reload files changed outside vim
let mapleader=" "
syntax on " turn on syntax highlighting
set nojoinspaces

set timeout timeoutlen=1000 ttimeoutlen=100 " Fix slow O inserts
set updatetime=100 " Make vim-gitgutter update faster

set wildignore+=node_modules/**
set wildignore+=vendor/**
set wildignore+=*.beam

" ================ Buffer & Windows =================

set hidden

" ================ Splits =================

set splitright
set splitbelow
autocmd FileType help wincmd L " Always open help in a vertical split

" ================ Folds =================

set viewoptions=cursor,folds,slash,unix
set foldmethod=syntax
set foldlevel=999
nnoremap <leader>z za

" ================ Search Settings =================

set incsearch	" Find the next match as we type the search
set hlsearch	" Hilight searches by default
set ignorecase	" Case insensitive
set smartcase	" Searches with uppercase characters are case-sensitive

" ================ Disable backup files ================

set nobackup
set nowritebackup

" ================ Swap ================

if isdirectory($HOME . '/.vim-swap') == 0
  :silent !mkdir -p ~/.vim-swap > /dev/null 2>&1
endif
set directory=~/.vim-swap//

" ================ Undo ================

if isdirectory($HOME . '/.vim-undo') == 0
  :silent !mkdir -p ~/.vim-undo > /dev/null 2>&1
endif
set undodir=~/.vim-undo//
set undofile

" ================ Indentation ======================

set expandtab
set shiftwidth=2
set tabstop=2
set softtabstop=2

" visible whitespace
set list listchars=tab:»·,trail:·

filetype plugin on
filetype indent on

" ================ Scrolling ========================

set scrolloff=999
set sidescrolloff=15
set sidescroll=1

" ================ Reload .vimrc on save ========================

autocmd! bufwritepost .vimrc source %

" ================ External config for plugins ========================

colorscheme molokai
" allow transparent background
hi Normal ctermfg=252 ctermbg=none
hi NonText ctermfg=252 ctermbg=none

" disable built-in plugin that highlights matching parens
let loaded_matchparen=1
" make % work for html
runtime macros/matchit.vim

" ================ Personal functions ========================

function! IndentXml()
	%s/></>\r</g
	normal! gg=G
endfunction
command! IndentXml call IndentXml()

" edit contents of a register, useful for tweaking macros
fun! ChangeReg() abort
  let x = nr2char(getchar())
  call feedkeys("q:ilet @" . x . " = \<c-r>\<c-r>=string(@" . x . ")\<cr>\<esc>0f'", 'n')
endfun
nnoremap cr :call ChangeReg()<cr>

function! FormatJson()
  %!python -m json.tool
  normal! gg=G
endfunction
command! FormatJson call FormatJson()
