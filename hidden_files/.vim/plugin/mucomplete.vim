set completeopt=menuone,noinsert
set shortmess+=c   " Shut off completion messages
set belloff+=ctrlg " If Vim beeps during completion

let g:mucomplete#enable_auto_at_startup = 1
let g:mucomplete#chains = {}
let g:mucomplete#chains.default = ['omni', 'c-p', 'tags', 'dict', 'spel']
let g:mucomplete#chains.ruby = []
let g:mucomplete#chains.sql = ['c-p', 'tags', 'dict', 'spel']
