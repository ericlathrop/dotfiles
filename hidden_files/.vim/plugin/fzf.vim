let g:fzf_history_dir = '~/.fzf-history'

nnoremap <C-p> :FZF<CR>
