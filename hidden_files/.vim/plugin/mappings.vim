" map capital Write/Quit to lower-case
command! WQ wq
command! Wq wq
command! W w
command! Q q

nnoremap <leader>q :bd<CR>
nnoremap <leader>w :w<CR>

" ================ Clipboard ====================

" make Y act like C and D
nnoremap Y y$

nnoremap <leader>y "+y
nmap <leader>Y "+Y
nnoremap <leader>p "+p
nnoremap <leader>P "+P
nnoremap <leader><leader>y "*y
nmap <leader><leader>Y "*Y
nnoremap <leader><leader>p "*p
nnoremap <leader><leader>P "*P
vnoremap <leader>y "+y
vmap <leader>Y "+Y
vnoremap <leader>p "+p
vnoremap <leader>P "+P
vnoremap <leader><leader>y "*y
vmap <leader><leader>Y "*Y
vnoremap <leader><leader>p "*p
vnoremap <leader><leader>P "*P

" ================ Buffers ====================

nnoremap <leader>o :only<CR>
nnoremap <Left> :bprev<CR>
nnoremap <Right> :bnext<CR>

nnoremap <leader>t :%s/\s\+$//e<CR>
