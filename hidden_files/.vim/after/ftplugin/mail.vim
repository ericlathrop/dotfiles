setlocal textwidth=0 " disable hard wrap

setlocal wrap " enable soft wrapping
setlocal linebreak " don't break words when wrapping
setlocal nolist " linebreak doesn't work with the list option enabled
setlocal columns=80

setlocal spell
