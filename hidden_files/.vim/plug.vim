call plug#begin('~/.vim/plugged')

Plug 'tpope/vim-repeat'
Plug 'tpope/vim-abolish'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-eunuch'
Plug 'tpope/vim-ragtag'
Plug 'tpope/vim-endwise'
Plug 'rstacruz/vim-closer'
Plug '~/packages/fzf'
Plug 'bronson/vim-visual-star-search'
Plug 'w0rp/ale'
" Plug 'SirVer/ultisnips'
Plug 'lifepillar/vim-mucomplete'
Plug 'drzel/vim-split-line'
Plug 'alvan/vim-closetag'
Plug 'AndrewRadev/splitjoin.vim'
Plug 'editorconfig/editorconfig-vim'

" UI
Plug 'ap/vim-buftabline'
Plug 'unblevable/quick-scope'
Plug 'junegunn/rainbow_parentheses.vim'
Plug 'vim-scripts/restore_view.vim'
Plug 'christoomey/vim-tmux-navigator'

" color scheme
Plug 'tomasr/molokai'

" git support
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'
Plug 'shumphrey/fugitive-gitlab.vim'

" Language support
Plug 'pangloss/vim-javascript'
Plug 'elzr/vim-json'
Plug 'MaxMEllon/vim-jsx-pretty'

Plug 'kana/vim-textobj-user'
Plug 'amiralies/vim-textobj-elixir'
Plug 'elixir-editors/vim-elixir'
Plug 'mhinz/vim-mix-format'

Plug 'vim-ruby/vim-ruby'
Plug 'ericpruitt/tmux.vim'
Plug 'mustache/vim-mustache-handlebars'
Plug 'chrisbra/csv.vim'
Plug 'ekalinin/Dockerfile.vim'
Plug 'tikhomirov/vim-glsl'
Plug 'hashivim/vim-terraform'
Plug 'cespare/vim-toml'
Plug 'pearofducks/ansible-vim'

Plug 'OrangeT/vim-csharp'
Plug 'OmniSharp/omnisharp-vim'

call plug#end()
