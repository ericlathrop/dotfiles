IEx.configure(
  colors: [eval_result: [:cyan, :bright]],
  inspect: [limit: :infinity, printable_limit: :infinity, width: 0]
)
