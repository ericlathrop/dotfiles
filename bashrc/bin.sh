if [ -d "$HOME/.dotfiles/bin" ] ; then
  PATH="$HOME/.dotfiles/bin:$PATH"
fi
if [ -d "$HOME/bin" ] ; then
  PATH="$HOME/bin:$PATH"
fi
if [ -f "$HOME/.bins" ]; then
  while read bin; do
    PATH="$bin:$PATH"
  done <$HOME/.bins
fi
