export EDITOR="vim"
if command -v nvim &> /dev/null; then
  export EDITOR="nvim"
  alias vi="nvim"
  alias vim="nvim"
fi
