if [ -f "$HOME/.local/bin/virtualenvwrapper.sh" ]; then
  export WORKON_HOME=$HOME/.virtualenvs
  export PROJECT_HOME=$HOME/src
  source $HOME/.local/bin/virtualenvwrapper.sh
fi
