# Setup fzf
# ---------
if [[ ! "$PATH" == */home/eric/packages/fzf/bin* ]]; then
  export PATH="${PATH:+${PATH}:}/home/eric/packages/fzf/bin"
fi

# Auto-completion
# ---------------
[[ $- == *i* ]] && source "/home/eric/packages/fzf/shell/completion.bash" 2> /dev/null

# Key bindings
# ------------
source "/home/eric/packages/fzf/shell/key-bindings.bash"

export FZF_TMUX=1
