alias awscreds="vim $HOME/.aws/credentials"

if [ -f '/usr/local/bin/aws_completer' ]; then
	complete -C '/usr/local/bin/aws_completer' aws
fi
