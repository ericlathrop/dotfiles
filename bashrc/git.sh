if [ -f /usr/share/bash-completion/completions/git ]; then
  source /usr/share/bash-completion/completions/git
fi

alias ga='git add'
alias ga.='git add .'
alias gap='git add -p'
alias gau='git add -u'

alias gb='git branch'
alias gba='git branch -a'
alias gbd='git branch -d'

alias gcm='git commit'

alias gco='git checkout'
alias gcb='git checkout -b'
alias gcp='git checkout -p'

alias gd='git diff'
alias gd.='git diff .'
alias gdw='git diff -w'
alias gds='git diff --staged'
alias gdt='git difftool'

alias gf='git fetch'

alias grp='git reset -p'

alias gs='git status -sb'

function gr() {
  cd "$(git rev-parse --show-toplevel)" || return
}
