if [ -f "$HOME/.libs" ]; then
	while read lib; do
		LD_LIBRARY_PATH="$lib:$LD_LIBRARY_PATH"
	done <$HOME/.libs
	export LD_LIBRARY_PATH
fi
