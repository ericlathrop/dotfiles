export ERL_AFLAGS="-kernel shell_history enabled"

alias exdocs="mix hex.docs offline"

no_deps() {
  local project_root=$1

  [ ! -r "$project_root/deps" ]
}

complete_exdocs() {
  local project_root=$(pwd -P)

  while no_deps "$project_root"; do
    if [ "$project_root" = '/' ]; then
      return 1
    fi

    project_root=$(dirname "$project_root")
  done

  if [ "$COMP_CWORD" != '1' ]; then
    return 1
  fi

  local CURR_ARG
  local FILES=$(find "$project_root/deps" -maxdepth 1 -type d | tail -n +2 | xargs basename --multiple)

  CURR_ARG=${COMP_WORDS[COMP_CWORD]}
  COMPREPLY=( $(compgen -W "${FILES[@]}" -- $CURR_ARG ) );
}

complete -o default -F complete_exdocs exdocs
